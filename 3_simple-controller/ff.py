'''
Coursera:
- Software Defined Networking (SDN) course
-- Programming Assignment: Layer-2 Firewall Application

Professor: Nick Feamster
Teaching Assistant: Arpit Gupta
'''

from pox.core import core
import pox.openflow.libopenflow_01 as of
from pox.lib.revent import *
from pox.lib.util import dpidToStr
from pox.lib.addresses import EthAddr
from collections import namedtuple
from pox.lib.util import dpid_to_str
from pox.lib.util import str_to_bool
import os
''' Add your imports here ... '''

from pox.lib.addresses import EthAddr

log = core.getLogger()
policyFile = "%s/pox/pox/misc/firewall-policies.csv" % os.environ[ 'HOME' ]

''' Add your global variables here ... '''

csvfile = file(policyFile,'rb')
# reader = csv.reader(csvfile)

_flood_delay = 0


class Firewall (EventMixin):

    def __init__ (self):
        self.listenTo(core.openflow)
        log.debug("Enabling Firewall Module")

    def _handle_ConnectionUp (self, event):
        ''' Add your logic here ... '''

       # Our firewall table
        self.firewall = {}

        # Add a Couple of Rules
        self.AddRule('00-00-00-00-00-01',EthAddr('00:00:00:00:00:01'))
        self.AddRule('00-00-00-00-00-01',EthAddr('00:00:00:00:00:02'))

        # Get the DPID of the Switch Connection
        dpidstr = dpid_to_str(event.connection.dpid)


        msg = of.ofp_flow_mod()        #want to right a flow
        msg.match.dl_src = EthAddr("00:00:00:00:00:01")      #matching the source mac address in the policy file
        msg.match.dl_dst = EthAddr("00:00:00:00:00:02")      #matching the destination mac address in the policy file
        msg.priority = 100     # set the priority over the normal forwarding flow
        event.connection.send(msg)  # send the flow entry


        log.debug("Firewall rules installed on %s", dpidToStr(event.dpid))

     # function that allows adding firewall rules into the firewall table
    def AddRule (self, dpidstr, src=0,value=True):
        self.firewall[(dpidstr,src)]=value
        log.debug("Adding firewall rule in %s: %s", dpidstr, src) 
    
      # function that allows deleting firewall rules from the firewall table
    def DeleteRule (self, dpidstr, src=0):
         try:
           del self.firewall[(dpidstr,src)]
           log.debug("Deleting firewall rule in %s: %s",
                     dpidstr, src)
         except KeyError:
           log.error("Cannot find in %s: %s",
                     dpidstr, src)
    
    
      # check if packet is compliant to rules before proceeding
    def CheckRule (self, dpidstr, src=0):
        try:
          entry = self.firewall[(dpidstr, src)]
          if (entry == True):
            log.debug("Rule (%s) found in %s: FORWARD",
                      src, dpidstr)
          else:
            log.debug("Rule (%s) found in %s: DROP",
                      src, dpidstr)
          return entry
        except KeyError:
          log.debug("Rule (%s) NOT found in %s: DROP",
                    src, dpidstr)
          return False
    
def launch ():
    '''
    Starting the Firewall module
    '''
    core.registerNew(Firewall)

