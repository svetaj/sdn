'''
Coursera:
- Software Defined Networking (SDN) course
-- Programming Assignment: Layer-2 Firewall Application

Professor: Nick Feamster
Teaching Assistant: Arpit Gupta
'''

from pox.core import core
import pox.openflow.libopenflow_01 as of
from pox.lib.revent import *
from pox.lib.util import dpidToStr
from pox.lib.addresses import EthAddr
from collections import namedtuple
from pox.lib.util import dpid_to_str
from pox.lib.util import str_to_bool
import os
''' Add your imports here ... '''

from pox.lib.addresses import EthAddr
import csv

log = core.getLogger()
policyFile = "%s/pox/pox/misc/firewall-policies.csv" % os.environ[ 'HOME' ]

''' Add your global variables here ... '''
csvfile = open(policyFile)

_flood_delay = 0

class Firewall (EventMixin):

    def __init__ (self):
        self.listenTo(core.openflow)
        log.debug("Enabling Firewall Module")

    def _handle_ConnectionUp (self, event):
        ''' Add your logic here ... '''

        reader = csv.DictReader(csvfile)
        for row in reader:
            msg = of.ofp_flow_mod()        
            msg.match.dl_src = EthAddr(row['mac_0'])    
            msg.match.dl_dst = EthAddr(row['mac_1'])   
            #msg.match.dl_src = EthAddr("00:00:00:00:00:01")    
            #msg.match.dl_dst = EthAddr("00:00:00:00:00:03")   
            msg.priority = 100     
            event.connection.send(msg)  

        log.debug("Firewall rules installed on %s", dpidToStr(event.dpid))

def launch ():
    '''
    Starting the Firewall module
    '''
    core.registerNew(Firewall)

