'''
    Coursera:
    - Software Defined Networking (SDN) course
    -- Module 6 Programming Assignment

    Professor: Nick Feamster
    Teaching Assistant: Muhammad Shahbaz
'''

################################################################################
# The Pyretic Project                                                          #
# frenetic-lang.org/pyretic                                                    #
# author: Joshua Reich (jreich@cs.princeton.edu)                               #
################################################################################
# Licensed to the Pyretic Project by one or more contributors. See the         #
# NOTICES file distributed with this work for additional information           #
# regarding copyright and ownership. The Pyretic Project licenses this         #
# file to you under the following license.                                     #
#                                                                              #
# Redistribution and use in source and binary forms, with or without           #
# modification, are permitted provided the following conditions are met:       #
# - Redistributions of source code must retain the above copyright             #
#   notice, this list of conditions and the following disclaimer.              #
# - Redistributions in binary form must reproduce the above copyright          #
#   notice, this list of conditions and the following disclaimer in            #
#   the documentation or other materials provided with the distribution.       #
# - The names of the copyright holds and contributors may not be used to       #
#   endorse or promote products derived from this work without specific        #
#   prior written permission.                                                  #
#                                                                              #
# Unless required by applicable law or agreed to in writing, software          #
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT    #
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the     #
# LICENSE file distributed with this work for specific language governing      #
# permissions and limitations under the License.                               #
################################################################################

from pox.lib.addresses import EthAddr
from pyretic.lib.corelib import *
from pyretic.lib.std import *
import os
import csv


# insert the name of the module and policy you want to import
from pyretic.examples.mac_learner import mac_learner
policy_file = "%s/pyretic/pyretic/examples/firewall-policies.csv" % os.environ[ 'HOME' ]

class firewall(DynamicPolicy):

    def __init__(self):
        # Initialize the firewall
        print "initializing firewall"
        self.firewall = {}
        super(firewall,self).__init__(true)
        import threading
        #self.ui = threading.Thread(target=self.ui_loop)
        #self.ui.daemon = True
        #self.ui.start()


    # Copy the code you used to read firewall-policies.csv last week

    # start with a policy that doesn't match any packets
        not_allowed = none
    # and add traffic that isn't allowed

        csvfile = open(policy_file)
        reader = csv.DictReader(csvfile)
        for row in reader:
             mac1 = EthAddr(row['mac_0'])
             mac2 = EthAddr(row['mac_1'])
             self.firewall[(mac1,mac2)]=True
             #not_allowed = not_allowed | ((match(srcmac=mac1) & match(dstmac=mac2)) | ((match(dstmac=mac1) & match(srcmac=mac2))
        self.policy = ~union([ (match(srcmac=mac1) &
                                match(dstmac=mac2)) |
                               (match(dstmac=mac1) &
                                match(srcmac=mac2))
                               for mac1, mac2
                               in self.firewall.keys()])


    #for <each pair of MAC address in firewall-policies.csv>:
    #    not_allowed = not_allowed + ( <traffic going in one direction> ) + ( <traffic going in the other direction> )

    # express allowed traffic in terms of not_allowed - hint use '~'

    # and only send allowed traffic to the mac learning (act_like_switch) logic
def main():
    return firewall() >> mac_learner()



