#!/usr/bin/python
'''
Coursera:
- Software Defined Networking (SDN) course
-- Programming Assignment 2

Professor: Nick Feamster
Teaching Assistant: Arpit Gupta, Muhammad Shahbaz
'''

from mininet.topo import Topo
from mininet.net import Mininet
from mininet.util import irange,dumpNodeConnections
from mininet.log import setLogLevel
from mininet.link import TCLink


class CustomTopo(Topo):
    "Simple Data Center Topology"

    "linkopts - (1:core, 2:aggregation, 3: edge) parameters"
    "fanout - number of child switch per parent switch"
    def __init__(self, linkopts1, linkopts2, linkopts3, fanout=2, **opts):
        # Initialize topology and default options
        Topo.__init__(self, **opts)
        
        # Add your logic here ...
        coresw = self.addSwitch('c%s' % 1)
        ii=1
        jj=1
        kk=1
        for i in irange(1, fanout):
           agresw = self.addSwitch('a%s' % ii)
           ii=ii+1
           self.addLink( coresw, agresw, **linkopts1)
           for j in irange(1, fanout):
               edgesw = self.addSwitch('e%s' % jj)
               jj=jj+1
               self.addLink( agresw, edgesw, **linkopts2)
               for k in irange(1, fanout):
                   host = self.addHost('h%s' % kk)
                   kk=kk+1
                   self.addLink( edgesw, host, **linkopts3)

        
topos = { 'custom': ( lambda: CustomTopo() ) }

def topoTest():
   linkopts1 = {'bw':50, 'delay':'5ms'}
   linkopts2 = {'bw':30, 'delay':'10ms'}
   linkopts3 = {'bw':10, 'delay':'15ms'}

   fanout = 2
   topo = CustomTopo(linkopts1, linkopts2, linkopts3, fanout)
   net = Mininet(topo=topo, link=TCLink)
   net.start()
   print "Dumping host connections"
   dumpNodeConnections(net.hosts)
   net.pingAll()
   net.stop()

if __name__ == '__main__':
   setLogLevel('info')
   topoTest() 
